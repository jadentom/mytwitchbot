# MyTwitchBot

GOALS:

- Database of all messages
  - Make it SQL queryable through chat
- Autodetect pastas
  - Use Reddit to post them to a subreddit
- !song
  - Donation page, bit donations, last link posted by broadcaster, last link posted in chat by anyone

Functionality so far:

- Puts all messages into a LiteDB db

Immediate TODO:

- Read credentials from file
- Query for broadcaster's last message
- UserName???