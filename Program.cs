﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CommandLine;
using LiteDB;
using TwitchLib.Client;
using TwitchLib.Client.Enums;
using TwitchLib.Client.Events;
using TwitchLib.Client.Extensions;
using TwitchLib.Client.Models;
using TwitchLib.Client.Models.Internal;

namespace MyTwitchBot
{
    class Program
    {
        const string DBPath = @"TwitchMessageLog.db";
        const string TableName = @"MessagesTable";

        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args)
                .WithParsed<Options>(o =>
                {
                    using(var db = new LiteDatabase(DBPath))
                    {
                        var messages = db.GetCollection<DBChatMessage>(TableName);
                        var bot = new Bot(o.Username, o.Token, o.Channel, messages);
                        var printTask = PrintDBStatsAsync(db, messages);

                        Console.WriteLine("Type 'exit' to exit");
                        while (Console.ReadLine() != "exit");
                    }
                });
        }

        class Options
        {
            [Option('u', "Username", Required = true, HelpText = "Bot username.")]
            public string Username { get; set; }
            
            [Option('t', "Token", Required = true, HelpText = "Bot authentication token.")]
            public string Token { get; set; }
            
            [Option('c', "Channel", Required = true, HelpText = "Bot channel.")]
            public string Channel { get; set; }
            
            [Option('v', "verbose", Required = false, HelpText = "Set output to verbose messages.")]
            public bool Verbose { get; set; }
        }

        static async Task PrintDBStatsAsync(LiteDatabase db, LiteCollection<DBChatMessage> messages)
        {
            var bsonMapper = new BsonMapper();
            var recentPeriod = TimeSpan.FromMinutes(100000);
            while (true)
            {
                var fileInfo = new FileInfo(DBPath);
                var fileSize = fileInfo.Exists ? fileInfo.Length : -1;
                var messageCount = 0;
                IEnumerable<DBChatMessage> recentMessages = null;
                lock (messages)
                {
                    messageCount = messages.Count();
                    recentMessages = messages.Find(m => m.DateTime > DateTime.UtcNow - recentPeriod);
                }
                
                var lastMessage = recentMessages.OrderByDescending(m => m.DateTime).FirstOrDefault();
                Console.WriteLine($"Stats update @{DateTime.UtcNow}\n" +
                    $"File size:            {fileSize} bytes\n" +
                    $"Message count:        {messageCount}\n" +
                    $"Last message date:    {lastMessage?.DateTime.ToString() ?? "<NO RECENT MESSAGE>"}\n" +
                    $"Last message user:    {lastMessage?.Username ?? "<NO RECENT MESSAGE>"}\n" +
                    $"Last message:         {lastMessage?.Message ?? "<NO RECENT MESSAGE>"}\n");

                await(Task.Delay(TimeSpan.FromSeconds(10)));
            }
        }

        class Bot
        {
            TwitchClient client;
            LiteCollection<DBChatMessage> messages;

            public Bot(string username, string accessToken, string channel, LiteCollection<DBChatMessage> messages)
            {
                this.messages = messages;
                ConnectionCredentials credentials = new ConnectionCredentials(username, accessToken);

                client = new TwitchClient();
                client.Initialize(credentials, channel);

                client.OnConnected += OnConnected;
                client.OnMessageReceived += OnMessageReceived;
                client.OnWhisperReceived += OnWhisperReceived;
                
                client.Connect();
            }

            private void OnConnected(object sender, OnConnectedArgs e)
            {
                Console.WriteLine($"Connected to {e.AutoJoinChannel}");
            }

            private void OnMessageReceived(object sender, OnMessageReceivedArgs e)
            {
                lock (messages)
                {
                    messages.Insert(new DBChatMessage(e.ChatMessage));
                }
            }

            private void OnWhisperReceived(object sender, OnWhisperReceivedArgs e)
            {
                if (e.WhisperMessage.Username == "my_friend")
                    client.SendWhisper(e.WhisperMessage.Username, "Hey! Whispers are so cool!!");
            }
        }
    }
}
