using System;
using System.Collections.Generic;
using System.Drawing;
using TwitchLib.Api.Core.Enums;
using TwitchLib.Client.Models;

namespace MyTwitchBot
{
    /// <summary>
    /// Class for chat messages in database, repackages <see cref="ChatMessage"/>
    /// for LiteDB Bson document storage (serialization?)
    /// Does not handle cheer badges
    /// </summary>
    class DBChatMessage
    {
        // From ChatMessage
        public string RawIrcMessage { get; set; }
        public Noisy Noisy { get; set; }
        public string Message { get; set; }
        public bool IsSubscriber { get; set; }
        public bool IsModerator { get; set; }
        public bool IsMe { get; set; }
        public string MessageId { get; set; }
        public string RoomId { get; set; }
        public string EmoteReplacedMessage { get; set; }
        public string Channel { get; set; }
        public double BitsInDollars { get; set; }
        public int Bits { get; set; }
        public bool IsBroadcaster { get; set; }
        public int SubscribedMonthCount { get; set; }

        // From TwitchLibMessage
        public List<KeyValuePair<string, string>> Badges { get; set; }
        public string BotUsername { get; set; }
        public Color Color { get; set; }
        public string ColorHex { get; set; }
        public string DisplayName { get; set; }
        public EmoteSet EmoteSet { get; set; }
        public bool IsTurbo { get; set; }
        public string UserId { get; set; }
        public string Username { get; set; }
        public UserType UserType { get; set; }

        // Not from ChatMessage
        public int Id { get; set; }
        public DateTime DateTime { get; set; }

        /// <summary>
        /// Empty constructor for LiteDB
        /// </summary>
        public DBChatMessage()
        {
        }

        public DBChatMessage(ChatMessage message)
        {
            this.RawIrcMessage = message.RawIrcMessage;
            this.Noisy = (Noisy)message.Noisy;
            this.Message = message.Message;
            this.IsSubscriber = message.IsSubscriber;
            this.IsModerator = message.IsModerator;
            this.IsMe = message.IsMe;
            this.MessageId = message.Id;
            this.RoomId = message.RoomId;
            this.EmoteReplacedMessage = message.EmoteReplacedMessage;
            this.Channel = message.Channel;
            this.BitsInDollars = message.BitsInDollars;
            this.Bits = message.Bits;
            this.IsBroadcaster = message.IsBroadcaster;
            this.SubscribedMonthCount = message.SubscribedMonthCount;

            this.Badges = message.Badges;
            this.BotUsername = message.BotUsername;
            this.Color = message.Color;
            this.ColorHex = message.ColorHex;
            this.DisplayName = message.DisplayName;
            this.EmoteSet = message.EmoteSet;
            this.IsTurbo = message.IsTurbo;
            this.UserId = message.UserId;
            this.Username = message.Username;
            this.UserType = (UserType)message.UserType;

            DateTime = DateTime.UtcNow;
            // Do not initialize ID because it LiteDB will do it.
        }
    }
}